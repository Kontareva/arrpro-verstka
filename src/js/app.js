import $ from 'jquery';
import 'slick-carousel';

$(document).ready(function() {
  $('#teamSlider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '.arrow-next',
    prevArrow: '.arrow-prev',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('#successSlider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '.successSlider_next',
    prevArrow: '.successSlider_prev',
    dots: true,
    appendDots: '.successSlider__paging',
    customPaging: function(slider, i) {
      return '<span class="bold">' + (i + 1) + '</span>' + '/' + slider.slideCount;
    }
  });

  $('#expertsSlider').slick({
    fade: true,
    speed: 1000,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '.expertsSlider_next',
    prevArrow: '.expertsSlider_prev',
    dots: true,
    dotsClass: 'expertsSlider_paging',
    customPaging: function(slider, i) {
      return '<span class="bold red">' + (i + 1) + '</span>' + '/' + slider.slideCount;
    }
  });

  $('#newsSlider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '.newsSlider__next',
    prevArrow: '.newsSlider__prev',
    dots: true,
    dotsClass: 'newsSlider_paging',
    customPaging: function(slider, i) {
      return '<span class="bold red">' + (i + 1) + '</span>' + '/' + slider.slideCount;
    }
  });

  $('#membersSlider').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '.membersSlider__next',
    prevArrow: '.membersSlider__prev',
    dots: true,
    dotsClass: 'membersSlider_paging',
    customPaging: function(slider, i) {
      return '<span class="bold red">' + (i + 1) + '</span>' + '/' + slider.slideCount;
    }
  });

  $('#certificateSlider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: '.certificate__next',
    prevArrow: '.certificate__prev',
    dots: true,
    appendDots: '.certificate__page',
    customPaging: function(slider, i) {
      return '<span class="bold red">' + (i + 1) + '</span>' + '/' + slider.slideCount;
    },
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('#photoSlider_for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '#photoSlider_nav',
    autoplay: true,
    nextArrow: '.photoSlider__next',
    prevArrow: '.photoSlider__prev',
  });
  $('#photoSlider_nav').slick({
    slidesToShow: 10,
    slidesToScroll: 1,
    asNavFor: '#photoSlider_for',
    dots: false,
    arrows: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 8,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 767,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 575,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    }]
  });

  //Dropdown-menu
  $('.navBottom__dropdown').hide();

  $('.navBottom__item').hover(function() {
    $(this).find('.navBottom__dropdown').slideDown({
      start: function() {
        $(this).css('display', 'flex');
      }
    });
  }, function() {
    $(this).find('.navBottom__dropdown').slideUp();
  } );


  // Выпадающее меню в фильтре, значение - текст

  $('.dropdown-menu li a').click(function(e) {
    e.preventDefault();
    let selText = $(this).text();
    $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
    $(this).parents('.btn-group').find('input[type="hidden"]').attr('value', selText);
    checkDealType();
  });


  //Выпадающее меню в фильтре, значение - data-id

  // $('.dropdown-menu li a').click(function(e) {
  //   e.preventDefault();
  //   var selText = $(this).text();
  //   var dataAttr = $(this).attr('data-id');
  //   $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
  //   $('input#propStage').attr('value', dataAttr);
  // });

  //Mobile dropdown-menu
  let mobHeight = window.innerHeight - 58 + 'px';
  $('.headerMob__collapse.collapse').css('max-height', mobHeight);


  //Catalog view type change
  $('.catalog__viewType').on('click', function() {
    $('.catalog__viewType').removeClass('active');
    $(this).addClass('active');
    if($('#groupView').hasClass('active')) {
      $('.props').addClass('props_tiles');
    } else {
      $('.props').removeClass('props_tiles');
    }
  });

  $('.customPills__link').on('click', function() {
    $('.customPills__link').removeClass('active');
    $(this).addClass('active');
  });

  $('.props__imgBtn').on('click', function() {
    $(this).toggleClass('active');
  });

  $('#cityModalFilter a').on('click', function(e) {
    e.preventDefault();
    let selText = $(this).text();
    $('#propertyRegion').html(selText);
    $('#region').attr('value', selText);
  });

  $('input#city_search').keyup(function() {
    let _val = $(this).val().toLowerCase();
    $('#cityModal .cityModal__columns a').each(function() {
      let _str = $(this).text().toLowerCase();
      let _a = _str.indexOf(_val);
      if(_a === -1) {
        $(this).addClass('hidden');
      } else{
        $(this).removeClass('hidden');
      }
    });
  });

  $('#cityModalFilter #city_search').keyup(function() {
    let _val = $(this).val().toLowerCase();
    $('#cityModalFilter .cityModal__columns li').each(function() {
      let _str = $(this).find('a').text().toLowerCase();
      let _a = _str.indexOf(_val);
      if(_a === -1) {
        $(this).closest('li').addClass('hidden');
      } else{
        $(this).closest('li').removeClass('hidden');
      }
    });
  });
  $('#cityModal #city_search').keyup(function() {
    let _val = $(this).val().toLowerCase();
    $('#cityModal .cityModal__columns li').each(function() {
      let _str = $(this).find('a').text().toLowerCase();
      let _a = _str.indexOf(_val);
      if(_a === -1) {
        $(this).closest('li').addClass('hidden');
      } else{
        $(this).closest('li').removeClass('hidden');
      }
    });
  });

  $('.dropdown-menu .form-check-label').click(function(e) {
    e.stopPropagation();
  });

  $('.form-check-input').change(function() {
    let defaultValue = $(this).closest('.btn-group').find('.dropdown-toggle').text().split(':')[0] + ': ';
    $(this).closest('.dropdown-menu').find(':checked').each(function() {
      defaultValue += ($(this).siblings('label').text()+ ', ');
    });
    $(this).closest('.btn-group').find('.dropdown-toggle').text(defaultValue.slice(0, -2)).prop('title', defaultValue.slice(0, -2));;
    console.log(defaultValue);
  });


  //Check deal type, if null - price is inactive

  const checkDealType = () => {
    if (!$('#dealType').attr('value')) {
      $('.filterInputJs').prop('disabled', true);
      // $('#priceTo').prop('disabled', true);
      $('#priceGroup').attr('title', 'Сначала выберите тип сделки');
    } else {
      $('.filterInputJs').prop('disabled', false);
      // $('#priceTo').prop('disabled', false);
      $('#priceGroup').removeAttr('title');
    }
  };

  checkDealType();

});
